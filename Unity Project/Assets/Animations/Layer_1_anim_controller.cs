﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Layer_1_anim_controller : MonoBehaviour {

	public Animator anim;
    public GameObject anim0;
	public GameObject anim2;
	public GameObject anim3;
	public GameObject anim4;
	public GameObject anim5;
	public GameObject anim6;
	public GameObject anim7;
    public GameObject anim8;

    Animator anim00;
	Animator anim22;
	Animator anim33;
	Animator anim44;
	Animator anim55;
	Animator anim66;
	Animator anim77;
    Animator anim88;
    
	private bool played;


	// Use this for initialization
	void Start () {

		anim = GetComponent<Animator>();
	    anim00 = anim0.GetComponent<Animator>();
		anim22 = anim2.GetComponent<Animator>();
		anim33 = anim3.GetComponent<Animator>();
		anim44 = anim4.GetComponent<Animator>();
		anim55 = anim5.GetComponent<Animator>();
		anim66 = anim6.GetComponent<Animator>();
		anim77 = anim7.GetComponent<Animator>();
        anim88 = anim8.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
	    if (GameStateManager.GameState == GameStateManager.GameStates.ChainReaction && !played)
        {
            anim.Play("Tension Release");
            anim00.Play("Time");
            anim88.Play("cam_target");
            played = true;
        }

		if (anim.GetCurrentAnimatorStateInfo (0).IsName ("TR_post"))
			anim22.Play ("Slow");

		if (anim22.GetCurrentAnimatorStateInfo (0).IsName ("Slow_post"))
			anim33.Play ("hebel");

		if (anim33.GetCurrentAnimatorStateInfo (0).IsName ("hebel_post"))
			anim44.Play ("Rolle");

		if (anim44.GetCurrentAnimatorStateInfo (0).IsName ("Rolle_post")) {
			anim55.Play ("Weightfall");
			anim66.Play ("Roperight");
			anim77.Play ("ropeleft");
			anim33.Play ("hebel2");
		    //GameStateRef.GameState = GameStateManager.GameStates.Playing;
		}

	    float time = anim22.GetCurrentAnimatorStateInfo(0).length;
        Debug.Log(time);
	}
}
