﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WagonController : MonoBehaviour
{
    public List<string> GapWagonKeyList;

    public void PopulateList()
    {
        foreach (var collectibleController in GetComponentsInChildren<CollectibleController>())
        {
            GapWagonKeyList.Add(collectibleController.GapWagonKey);
        }
    }

    public void ClearList()
    {
        GapWagonKeyList.Clear();
    }
}
