﻿using UnityEngine;

public class TouchInput : MonoBehaviour
{
    public GameObject IndicatorPrefab;
    private GameObject PlayerRef;
    private GameObject ShoppingCartRef;
    private GameObject TouchAnimRef;

    public float HoldToCollectTime;
    public float HoldToPlaceTime;
    private float Timer;
    private bool SnappedToObject;

    private float ScreenToWorldPlayerRefX;
    private float ScreenToWorldCollectibleRefX;
    private RaycastHit ClickRaycastHit;

    private Vector3 MousePointerLocation;

    private Dictionaries DictRef;
    private CollectibleManager ColllectibleManagerRef;
    private AutoWalkManager AutoWalkManagerRef;

    // Use this for initialization
    void Start()
    {
        PlayerRef = GameObject.FindWithTag("Player");
        ShoppingCartRef = GameObject.FindWithTag("Wagon");
        DictRef = GameObject.FindWithTag("Manager").GetComponent<Dictionaries>();
        TouchAnimRef = GameObject.FindGameObjectWithTag("TouchAnim");
        ColllectibleManagerRef = GameObject.FindGameObjectWithTag("Manager").GetComponent<CollectibleManager>();
        AutoWalkManagerRef = GameObject.FindGameObjectWithTag("Manager").GetComponent<AutoWalkManager>();
    }

    // Update is called once per frame
    void Update()
    {
        HandleTouchInput();
        CollectCollectible();
        PlaceCollectible();
        MousePointerLocation = Camera.main.ScreenToWorldPoint(Input.mousePosition + new Vector3(0, 0, -Camera.main.transform.position.z));
    }

    //* Instantiates a TouchIndicator object at mouse position and childs it to the Level object.
    //* If a TouchIndicator already exists in the scene, it is destroyed before the new one is instantiated.
    //* The object is created to create a point in worldspace for the player object to move towards that also stays at the same location relative 
    //  to the rotating Level object.
    //* The prefab has a SpriteRenderer component attached to have a visual representation of the touch input for testing and debugging purposes. 
    //* This SpriteRenderer component will not be part of the final build.
    public void CreateTouchIndicator(Vector3 instantiateLocation)
    {
        if (GameObject.FindGameObjectWithTag("TouchIndicator") != null)
            Destroy(GameObject.FindGameObjectWithTag("TouchIndicator"));
        var indicatorRef = Instantiate(IndicatorPrefab, instantiateLocation, Quaternion.identity);
        indicatorRef.transform.parent = GameObject.FindGameObjectWithTag("Level").transform;
    }

    void HandleTouchInput()
    {
        if ((Input.GetMouseButton(0) || Input.GetMouseButtonUp(0) || Input.GetMouseButtonDown(0)) && !SnappedToObject)
        {
            //Stop moving on mousedown
            if (Input.GetMouseButtonDown(0))
            {
                CreateTouchIndicator(PlayerRef.transform.position);
            }

            //Collecting Conditional
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out ClickRaycastHit) && ClickRaycastHit.collider.tag.Equals("Collectible"))
            {
                if (Timer >= HoldToCollectTime)
                {
                    //Is Collectible" Feedback
                    TouchAnimRef.transform.position = ClickRaycastHit.collider.gameObject.transform.position;
                    TouchAnimRef.GetComponent<Animator>().Play("Touch");

                    CreateTouchIndicator(ClickRaycastHit.collider.transform.position);
                    SnappedToObject = true;
                    Timer = 0;
                }

                Timer += Time.deltaTime;
            }
            //Placing Conditional
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out ClickRaycastHit) && ClickRaycastHit.collider.tag.Equals("GapInMachine") && ShoppingCartRef.GetComponent<WagonController>().GapWagonKeyList.Contains(ClickRaycastHit.collider.gameObject.GetComponent<CollectibleController>().GapWagonKey))
            {
                if (Timer >= HoldToPlaceTime)
                {
                    //Is Gap in Machine" Feedback
                    TouchAnimRef.transform.position = ClickRaycastHit.collider.gameObject.transform.position;
                    TouchAnimRef.GetComponent<Animator>().Play("Touch");

                    CreateTouchIndicator(ClickRaycastHit.collider.transform.position);
                    SnappedToObject = true;
                    Timer = 0;
                }
                else if (Input.GetMouseButtonUp(0))
                {
                    CreateTouchIndicator(MousePointerLocation);
                    Timer = 0;
                }

                Timer += Time.deltaTime;
            }

            else if (Input.GetMouseButtonUp(0))
            {
                CreateTouchIndicator(MousePointerLocation);
                Timer = 0;
            }

            else if (ClickRaycastHit.collider == null)
                Timer = 0;
        }
    }

    // To get the positions of player and object to update outside of the conditionals, I had to make this seperate method for
    // actually collecting the Collectible.
    void CollectCollectible()
    {
        if (SnappedToObject && ClickRaycastHit.collider.gameObject.tag.Equals("Collectible"))
        {
            ScreenToWorldPlayerRefX = Camera.main.ScreenToWorldPoint(PlayerRef.transform.position).x;
            ScreenToWorldCollectibleRefX = Camera.main.ScreenToWorldPoint(ClickRaycastHit.collider.transform.position).x;
            if (ScreenToWorldCollectibleRefX == ScreenToWorldPlayerRefX)
            {
                //TODO: Play "Collect Collectible" Animation
                //while (ANIMATION.time <= ANIMATION.length){ LEAVE EMPTY }
                //Place Collectible in Player Inventory
                GameObject wagonPrefab = Instantiate(DictRef.ColNameToWagonPrefabDict[ClickRaycastHit.collider.gameObject.GetComponent<CollectibleController>().Name], ShoppingCartRef.transform.position, Quaternion.identity);
                wagonPrefab.transform.parent = ShoppingCartRef.transform;
                Destroy(ClickRaycastHit.collider.gameObject); //Remove the collectible object after getting it's tag in the line above.
                SnappedToObject = false;
                //Refresh the List that contains the keys of the objects in the wagon
                ShoppingCartRef.GetComponent<WagonController>().ClearList();
                ShoppingCartRef.GetComponent<WagonController>().PopulateList();
            }
        }
    }


    void PlaceCollectible()
    {
        if (SnappedToObject && ClickRaycastHit.collider.gameObject.tag.Equals("GapInMachine"))
        {
            ScreenToWorldPlayerRefX = Camera.main.ScreenToWorldPoint(PlayerRef.transform.position).x;
            ScreenToWorldCollectibleRefX = Camera.main.ScreenToWorldPoint(ClickRaycastHit.collider.transform.position).x;
            if (ScreenToWorldCollectibleRefX == ScreenToWorldPlayerRefX)
            {
                //TODO: Play "Place Collectible" Animation
                //while (ANIMATION.time <= ANIMATION.length){ LEAVE EMPTY }
                //Instantiate the corresponding collectible into the gap, untag it to make it noninteractible
                GameObject collectiblePrefab = Instantiate(DictRef.GapNameToColDict[ClickRaycastHit.collider.gameObject.GetComponent<CollectibleController>().Name], ClickRaycastHit.collider.gameObject.transform.position, ClickRaycastHit.collider.gameObject.transform.rotation);
                collectiblePrefab.transform.parent = ClickRaycastHit.collider.gameObject.transform;
                collectiblePrefab.tag = "Untagged";
                //TODO: Remove Collectible in Player Inventory
                SnappedToObject = false;
                // toggle a "placed" bool to check whether all collectibles have been placed + go to start logic
                ClickRaycastHit.collider.gameObject.GetComponent<CollectibleController>().Placed = true;
                ColllectibleManagerRef.GetPlacedBools();
                AutoWalkManagerRef.GoToStart();
            }
        }
    }
}