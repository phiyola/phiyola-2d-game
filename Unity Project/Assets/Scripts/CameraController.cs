﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float FollowLerpSpeed;
    public float MinZoom; //Always Set to Default Camera Size
    public float MaxZoom;
    public float CamVerticalOffset;

    private float SizeDivider = 700f;
    private float SmoothTime = 0.5f;

    private Transform PlayerTransRef;
    private Transform CamTargetTransRef;
    public List<Transform> CamTargetList;
    private Camera Cam;
    private Vector3 Velocity;

    // Use this for initialization
    void Start()
    {
        PlayerTransRef = GameObject.FindGameObjectWithTag("Player").transform;
        //CamTargetTransRef = GameObject.FindGameObjectWithTag("CamTarget").transform;
        CamTargetList.Add(PlayerTransRef);
        //CamTargetList.Add(CamTargetTransRef);
        Cam = GetComponent<Camera>();

        MinZoom = Cam.orthographicSize;
    }

    // Update is called once per frame
	void Update ()
	{
	    CameraState();
        SetCameraZ();
	}

    void CameraState()
    {
        if (GameStateManager.GameState == GameStateManager.GameStates.Playing)
            SmoothFollowPlayer();
        // if (GameStateManager.GameState == GameStateManager.GameStates.ChainReaction)
        //     ChainReactionCam();
    }

    void SetCameraZ()
    {
        if (Cam.transform.position.z != -10f)
            Cam.transform.position = new Vector3(Cam.transform.position.x, Cam.transform.position.y, -10);
    }

    void SmoothFollowPlayer()
    {
        Cam.orthographicSize = MinZoom;
        transform.position = Vector3.Lerp(transform.position, PlayerTransRef.position, FollowLerpSpeed) + new Vector3(0,CamVerticalOffset,0);
    }

    // void ChainReactionCam()
    // {
    //     CamZoom();
    //     CamMovement();
    // }

    void CamZoom()
    {
        // interpoliert zwischen zwei werten
        float newZoom = Mathf.Lerp(MinZoom, MaxZoom, GetGreatestDistance() / SizeDivider);
        // interpoliert zwischen altem und neuem zoom, smoooth
        Cam.orthographicSize = Mathf.Lerp(Cam.orthographicSize, newZoom, Time.deltaTime);
    }

    void CamMovement()
    {
        Vector3 centerpoint = GetCenterPoint();
        Vector3 newPosition = centerpoint;
        // smoothdamp ( current position, target position, velocity, smoothing factor)
        this.transform.position = Vector3.SmoothDamp(transform.position, newPosition, ref Velocity, SmoothTime);
    }

    float GetGreatestDistance()
    {
        var bounds = new Bounds(CamTargetList[0].position, Vector3.zero);
        for (int i = 0; i < CamTargetList.Count; i++)
        {
            bounds.Encapsulate(CamTargetList[i].position);
        }
        return bounds.size.x;
    }

    Vector3 GetCenterPoint()
    {
        //bounds der objekte in der liste finden = bounds(square um verschi3edene objekte)
        var bounds = new Bounds(CamTargetList[0].position, Vector3.zero);
        for (int i = 0; i < CamTargetList.Count; i++)
        {
            bounds.Encapsulate(CamTargetList[i].position);
        }
        return bounds.center;
    }
}