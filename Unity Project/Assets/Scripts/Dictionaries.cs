﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Dictionaries : MonoBehaviour {

    public Dictionary<string, GameObject> ColNameToWagonPrefabDict = new Dictionary<string, GameObject>();
    public Dictionary<string, GameObject> GapNameToColDict = new Dictionary<string, GameObject>();

    //The following Arrays are public for testing purposes only.
    public Object[] CollectiblePrefabArray;
    public Object[] CollectibleInWagonPrefabArray;
    public Object[] CollectibleGapArray;
    public Object[] AnimationPrefabArray;

    void Start()
    {
        PopulateArrays();
        PopulateDicts();
    }

    void PopulateArrays()
    {
        CollectiblePrefabArray = Resources.LoadAll("CollectiblePrefabs");
        CollectibleInWagonPrefabArray = Resources.LoadAll("CollectibleInWagonPrefabs");
        CollectibleGapArray = Resources.LoadAll("CollectibleGaps");
        AnimationPrefabArray = Resources.LoadAll("Animations");
    }

    void PopulateDicts()
    {
        int collectibleCount = CollectiblePrefabArray.Length;

        //Collectible Name to Wagon Prefab GameObject
        for (int i = 0; i < collectibleCount; i++)
        {
            GameObject gO = (GameObject) CollectiblePrefabArray[i];
            string gOName = gO.GetComponent<CollectibleController>().Name;
            GameObject wagonGORef = (GameObject) CollectibleInWagonPrefabArray[i];
            ColNameToWagonPrefabDict.Add(gOName, wagonGORef);
        }

        //Gap Name to Collectible Prefab GameObject
        for (int i = 0; i < collectibleCount; i++)
        {
            GameObject gO = (GameObject) CollectiblePrefabArray[i];
            GameObject gapGO = (GameObject) CollectibleGapArray[i];
            string gapGOName = gapGO.GetComponent<CollectibleController>().Name;
            GapNameToColDict.Add(gapGOName,gO);
        }
    }
}
