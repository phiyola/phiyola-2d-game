﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AutoWalkManager : MonoBehaviour
{
    private GameObject ManagerRef;
    private CollectibleManager ColManagerRef;
    private TouchInput TouchInputRef;
    
	// Use this for initialization
	void Start () {
	    ManagerRef = GameObject.FindGameObjectWithTag("Manager");
	    ColManagerRef = ManagerRef.GetComponent<CollectibleManager>();
	    TouchInputRef = ManagerRef.GetComponent<TouchInput>();
	}
	
	public void GoToStart ()
	{
	    if (AllCollectiblesPlaced())
	    {
            Debug.Log("beep");
            TouchInputRef.CreateTouchIndicator(GameObject.FindGameObjectWithTag("ReactionStartPosition").transform.position);
	    }
	}

    public bool AllCollectiblesPlaced()
    {
        for (int i = 0; i < ColManagerRef.GapBoolList.Count; i++)
        {
            if (ColManagerRef.GapBoolList[i] == false)
                return false;
        }

        GameStateManager.GameState = GameStateManager.GameStates.ChainReaction;
        return true;
    }
}
