﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float Speed;
    public float KeyboardSpeed;
    public float GroundDistance;

    public int LayerToBeMasked;
    private int LayerMask;

    public RaycastHit2D RayHit;
    public Vector2 RayHitNormal;
	
	// Update is called once per frame
    void Update()
    {
        LayerMask = 1 << LayerToBeMasked; // to understand this line google "unity layermask bitshifting"
        KeyboardMove();
        GetRays();
        GlueToGround();
        TouchMove();
    }


    // GetAxisRaw returns either 1,0 or -1. This makes for less floaty movement. 
    // This Method is for testing purposes only and will not be part of the final build.
    void KeyboardMove()
    {
        if (Input.GetAxisRaw("Horizontal") > 0)
            transform.position += Vector3.right * KeyboardSpeed;
        if (Input.GetAxisRaw("Horizontal") < 0)
            transform.position += Vector3.left * KeyboardSpeed;
    }

    void TouchMove()
    {
        if (GameObject.FindGameObjectWithTag("TouchIndicator") != null)
        {
            Transform touchIndicatorRef = GameObject.FindGameObjectWithTag("TouchIndicator").transform;
            if (transform.position.x != touchIndicatorRef.position.x)
            //transform.position = Vector3.MoveTowards(transform.position, touchIndicatorRef.position, Speed * Time.deltaTime);
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(touchIndicatorRef.transform.position.x,transform.position.y,transform.position.z), Speed * Time.deltaTime );
        }
    }

    // Subtracts the distance from the player object's pivot to the first plane hit by a downwards raycast - a variable "buffer zone".
    // The buffer zone is required to make the player object move correctly over both convexely as well as concavely curved ground.
    void GlueToGround()
    {
        if (RayHit)
        {
            transform.position -= new Vector3(0, RayHit.distance - GroundDistance, 0);
        }
    }

    // Casts a Ray downwards from the player object's pivot and defines a bool that is true if the ray hits something on a defined Unity Layer as well as a
    // vector that contains the normal (the vector perpendicular to a plane) of the plane that was hit by the ray.
    // Both variables are accessed in the LevelRotate script.
    void GetRays()
    {
        RayHit = Physics2D.Raycast(transform.position, Vector2.down, Mathf.Infinity, LayerMask);
        RayHitNormal = RayHit.normal.normalized;
    }
}
