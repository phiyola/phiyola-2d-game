﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleManager : MonoBehaviour
{
    private GameObject[] GapArray;

    public List<bool> GapBoolList;

	public void GetPlacedBools()
	{
        GapBoolList.Clear();
	    GapArray = GameObject.FindGameObjectsWithTag("GapInMachine");
	    foreach (var gO in GapArray)
	    {
	        GapBoolList.Add(gO.GetComponent<CollectibleController>().Placed);
	    }
	}
}
