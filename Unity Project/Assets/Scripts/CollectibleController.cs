﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleController : MonoBehaviour
{
    public string Name;
    public string GapWagonKey;
    public bool Placed;
}
