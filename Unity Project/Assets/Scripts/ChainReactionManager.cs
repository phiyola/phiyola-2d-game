﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainReactionManager : MonoBehaviour
{

    public List<ChainPartController> ChainPartControllerList;

    public int CurrentChainIndex;

    public bool ChainReactAnimIsDone;

	// Use this for initialization
	void Start ()
	{
	    CurrentChainIndex = -1;
        TriggerNextAnimation();
	}

	public void TriggerNextAnimation ()
	{
	    CurrentChainIndex++;
	    if (CurrentChainIndex >= ChainPartControllerList.Count)
	    {
	        ChainReactAnimIsDone = true;
	    }

        ChainPartControllerList[CurrentChainIndex].TriggerAnimation();
	}
}
