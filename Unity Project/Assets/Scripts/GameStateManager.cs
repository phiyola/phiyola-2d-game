﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStateManager : MonoBehaviour {

    void Start()
    {
        GameState = GameStates.Playing;
    }

    void Update()
    {
        Restart(); //FIXME: only for testing. Will be removed in final build.
    }

    public enum GameStates
    {
        Playing,
        ChainReaction,
        LayerTransition
    }

    public static GameStates GameState;

    void Restart()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("Working");
        }
    }
}
