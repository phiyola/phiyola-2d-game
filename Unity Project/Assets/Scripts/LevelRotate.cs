﻿using UnityEngine;

public class LevelRotate : MonoBehaviour
{
    public float RotationSpeed;
    public float AngleTolerance;

    private GameObject Player;
    private GameObject Level;

    private Vector2 RayHitNormal;

	// Use this for initialization
	void Start ()
	{
		Player = GameObject.Find("Player");
	    Level = GameObject.Find("Level");
	}
	
	// Update is called once per frame
	void Update ()
	{
        Rotate();
	}

    //Rotates the Level object and everything childed to it so that the player is always standing upright in relation to the screen's bottom screen.
    // if the vector between the normal of the plane hit by the raycast and the unity defined Vector2.up (0,1) is larger than a defined tolerance angle
    // AND the aforementioned normal's x value points to the left, the level object and its children rotate clockwise. The same principle applies for
    // counterclockwise rotation. If the angle is within the defined tolerance, the level and its children do not rotate.
    void Rotate()
    {
        if (Player.GetComponent<PlayerController>().RayHit)
        {
            RayHitNormal = Player.GetComponent<PlayerController>().RayHitNormal;
            if (Vector2.Angle(Vector2.up, RayHitNormal) >= AngleTolerance && RayHitNormal.x < 0)
            {
                Level.transform.RotateAround(Player.transform.position, Vector3.back, Time.deltaTime * RotationSpeed);
            }
            if (Vector2.Angle(Vector2.up, RayHitNormal) >= AngleTolerance && RayHitNormal.x > 0)
            {
                Level.transform.RotateAround(Player.transform.position, Vector3.forward, Time.deltaTime * RotationSpeed);
            }
            else
            {
                Level.transform.Rotate(0, 0, 0);
            }
        }
    }
}
