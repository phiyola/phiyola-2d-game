﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchInputManager : MonoBehaviour 
{
	//FIELDS
	public static Vector3 TouchPosition;		 //Where was the screen touched?
	float TouchDuration;						 //How long has the current touch been going on?
	Touch Touch;								 //Stores information on the most recent touch input
	public static Collider2D[] TouchedObjects;
	
	public static bool InputLocked;

	//EVENTS
	public delegate void TouchAction();
	public static event TouchAction OnTap;
	public static event TouchAction OnHold;

	//METHODS
	private void Update()
	{
		if(GameManager.GameState == GameManager.GameStates.Playing)
			GetTouchInput();
		Debug.DrawRay(TouchPosition, Vector3.up, Color.green);
	}

	private void GetTouchInput() //Differentiates a short tap input from a touch and hold input.
	{
		if(Input.touchCount > 0 && !InputLocked)
		{
            TouchDuration += Time.deltaTime;
            Touch = Input.GetTouch(0);
			TouchPosition = GetWorldPositionOnPlane(Touch.position, 0);
 
            if(Touch.phase == TouchPhase.Ended && TouchDuration < Settings.Instance.MaxTapDuration){
                OnTap();
				TouchDuration = 0.0f;
			}
			else if (TouchDuration >= Settings.Instance.HoldDuration){
				GetTouchedObjects();
				OnHold();
				TouchDuration = 0.0f;
			}
		}
        else
            TouchDuration = 0.0f;
	}

	private void GetTouchedObjects() //Fills an Array with all colliders colliding with a circle around touch position.
	{
		TouchedObjects = Physics2D.OverlapCircleAll(TouchPosition, Settings.Instance.OverlapCircleRadius, 1 << Settings.Instance.ObjectsMaskedLayer); //
	}

	 public Vector3 GetWorldPositionOnPlane(Vector3 screenPosition, float z) //Returns a Position on a xy plane on a given z axis. ScreenToWorldPoint for Perspective Camera.
	 {
		Ray ray = Camera.main.ScreenPointToRay(screenPosition);
		Plane xy = new Plane(Vector3.forward, new Vector3(0, 0, z));
		float distance;
		xy.Raycast(ray, out distance);
		return ray.GetPoint(distance);
 	}
}
