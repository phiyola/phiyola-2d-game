﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{
	//FIELDS
	public static GameStates GameState;
	public enum GameStates{
		Playing,
		GoToStart,
		ChainReaction,
		StartScreen
	}

	//METHODS
	private void Start()
	{
		GameState = GameStates.Playing;
	}
}
