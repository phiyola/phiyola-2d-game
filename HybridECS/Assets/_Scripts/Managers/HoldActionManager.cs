﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldActionManager : MonoBehaviour {

	//SUBSCRIPTIONS
	private void OnEnable()
	{
		MouseInputManager.OnHold += InteractableQuery;
	}
	private void OnDisable()
	{
		MouseInputManager.OnHold -= InteractableQuery;
	}

	//EVENTS
	public delegate void InteractableAction();
	public static event InteractableAction OnInteractable;
	//public static event InteractableAction OnNonInteractable; //TODO: Uncomment this when the FeedbackManager subscribes to this event.

	//METHODS
	void InteractableQuery()
	{		
		foreach (var collider in MouseInputManager.TouchedObjects)
		{
			if(collider.gameObject.tag.Equals("Interactable") && !collider.gameObject.GetComponent<InteractableController>().Placed){
				MoveIndicatorManager.Position = collider.gameObject.transform.position;
				MouseInputManager.InputLocked = true; //TODO: Uncomment this when animations are in and can toggle this to false upon finished pick up/ place animation.
				OnInteractable();
			}
			else{
				Debug.Log("NonInteractable");
				//OnNonInteractable(); //TODO: Uncomment this when the FeedbackManager subscribes to this event.
			}
		}
	}
}
