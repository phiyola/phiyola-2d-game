﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStateManager : MonoBehaviour {
	public enum PlayerStates
	{
		Idle,
		Walking,
		Collecting,
		Placing
	}
	public static PlayerStates PlayerState;
	void Start () {
		PlayerState = PlayerStates.Idle;
	}
}
