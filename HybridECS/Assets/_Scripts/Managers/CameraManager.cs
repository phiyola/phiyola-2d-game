﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour 
{
	// Camera CamRef;
	// private void Start()
	// {
	// 	CamRef = Camera.main;
	// }
	private void Update()
	{
		if (GameManager.GameState == GameManager.GameStates.Playing){
			Camera.main.GetComponent<SmoothCameraController>().enabled = true;
			Camera.main.GetComponent<ReactionCameraController>().enabled = false;
		}
		if (GameManager.GameState == GameManager.GameStates.ChainReaction){
			Camera.main.GetComponent<SmoothCameraController>().enabled = false;
			Camera.main.GetComponent<ReactionCameraController>().enabled = true;
		}
	}
}
