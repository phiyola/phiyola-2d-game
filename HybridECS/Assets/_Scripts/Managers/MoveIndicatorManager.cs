﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveIndicatorManager : MonoBehaviour 
{
	//FIELDS
	public static Vector3 Position;
	public GameObject MoveIndicatorPrefab;
	
	//METHODS
	private void OnEnable()
	{
		MouseInputManager.OnTap += CreateMoveIndicator;
		HoldActionManager.OnInteractable += CreateMoveIndicatorAtPos;
	}

	private void OnDisable()
	{
		MouseInputManager.OnTap -= CreateMoveIndicator;
		HoldActionManager.OnInteractable -= CreateMoveIndicatorAtPos;
	}

	private void CreateMoveIndicator() //Instantiates an Indicator at touch position.
	{
		OnlyOneIndicator();
		var touchPos = MouseInputManager.TouchPosition + new Vector3(0,0,0);
		Instantiate(MoveIndicatorPrefab, touchPos, Quaternion.identity);
	}

	private void CreateMoveIndicatorAtPos() //Instantiates an Indicator at custom position.
	{
		OnlyOneIndicator();
		Instantiate(MoveIndicatorPrefab, Position, Quaternion.identity);
	}

	private void OnlyOneIndicator() //Destroys all existing indicators if any exist.
	{
		if (GameObject.FindGameObjectsWithTag("MoveIndicator") != null){
			foreach (var indicator in GameObject.FindGameObjectsWithTag("MoveIndicator"))
			{
				Destroy(indicator);
			}
		}
	}
}
