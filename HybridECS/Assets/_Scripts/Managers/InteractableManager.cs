﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableManager : MonoBehaviour
{
    //FIELDS
    private GameObject[] GapArray;
    public List<bool> GapBoolList;
    private bool AnimationStarted;
    
    //SUBSCRIPTIONS
    // private void OnEnable()
    // {
    //     InteractableActionManager.OnPlace += GetPlacedBools;
    //     InteractableActionManager.OnPlace += AllCollectiblesPlaced;
    // }
    // private void OnDisable()
    // {
    //     InteractableActionManager.OnPlace -= GetPlacedBools;
    //     InteractableActionManager.OnPlace -= AllCollectiblesPlaced;       
    // }

    //EVENTS
    // public delegate void AllObjectsPlacedAction();
    // public static AllObjectsPlacedAction OnAllObjectsPlaced;
    
    //METHODS
    private void Update()
    {
        GetPlacedBools();
        AllCollectiblesPlaced();
    }
	public void GetPlacedBools() //Populates a List with bools of gap objects in the scene that are true if the gap has been filled.
	{
        GapBoolList.Clear();
	    GapArray = GameObject.FindGameObjectsWithTag("Interactable");
	    foreach (var gO in GapArray)
	    {
	        GapBoolList.Add(gO.GetComponent<InteractableController>().Placed);
	    }
	}

	public void AllCollectiblesPlaced() //returns a bool that is true if all gaps in the scene have been filled.
    {
        for (int i = 0; i < GapBoolList.Count; i++)
        {
            if (GapBoolList[i] == false){
                break;
            }
            else{
                if (!AnimationStarted){
                    GameManager.GameState = GameManager.GameStates.GoToStart; //GAMESTATE
                    //OnAllObjectsPlaced();
                    AnimationStarted = true;
                }
            }
        }
    }
}
