﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableActionManager : MonoBehaviour 
{
	//FIELDS
	GameObject WagonRef;
	GameObject PlayerRef;
	GameObject MoveIndicatorRef;

	public enum InteractableTypes{
		Part,
		Gap,
		None
	}
	public InteractableTypes InteractableType;
	Collider2D Collider;

	//EVENTS
	// public delegate void PlaceAction();
	// public static event PlaceAction OnPlace;

	//METHODS
	private void OnEnable(){
		HoldActionManager.OnInteractable += PartOrGapQuery;
	}
	private void OnDisable(){
		HoldActionManager.OnInteractable -= PartOrGapQuery;
	}
	private void Start()
	{
		WagonRef = GameObject.FindGameObjectWithTag("Wagon");
		PlayerRef = GameObject.FindGameObjectWithTag("Player");
		InteractableType = InteractableTypes.None;
	}
	private void Update()
	{
		PlaceOrCollect();
	}
	private void PartOrGapQuery(){
		foreach (var collider in MouseInputManager.TouchedObjects)
		{
			if (collider.gameObject.GetComponent<InteractableController>().Type == InteractableController.Types.Part){
				Collider = collider;
				InteractableType = InteractableTypes.Part;
			}
			if (collider.gameObject.GetComponent<InteractableController>().Type == InteractableController.Types.Gap){
				Collider = collider;
				InteractableType = InteractableTypes.Gap;
			}
		}
	}
	private void PlaceOrCollect(){
		if (InteractableType == InteractableTypes.Part)
			Collect(Collider);
		if (InteractableType == InteractableTypes.Gap)
			Place(Collider);
		else{}
	}
	private void Place(Collider2D collider){
		Debug.Log("Place");
		if (PlayerRef.transform.position.x == collider.gameObject.transform.position.x){
			PlayerStateManager.PlayerState = PlayerStateManager.PlayerStates.Placing; //PLAYERSTATE
			if (PlayerAnimationController.PlayerAnimIsFinished){
				PlayerAnimationController.PlayerAnimIsFinished = false;
				PlayerStateManager.PlayerState = PlayerStateManager.PlayerStates.Idle; //PLAYERSTATE
				var key = collider.gameObject.GetComponent<InteractableController>().Key;
				if (WagonRef.GetComponent<WagonController>().GetKeyList().Contains(key)){
					var placedObject = Instantiate(Dictionaries.GapKeyToColDict[key], collider.transform.position, Quaternion.identity);
					placedObject.GetComponent<SpriteRenderer>().enabled = false;
					placedObject.GetComponent<InteractableController>().Placed = true;
					Destroy(collider.gameObject);
					Debug.Log(collider.gameObject);
					foreach (var interactableController in WagonRef.GetComponentsInChildren<InteractableController>())
					{
						if (interactableController.Key == key){
							Destroy(interactableController.gameObject);
						}
					}
				}
				MouseInputManager.InputLocked = false;
				InteractableType = InteractableTypes.None;
			}
		}
		//OnPlace();
	}
	private void Collect(Collider2D collider){
		Debug.Log("Collect");
		if (PlayerRef.transform.position.x == collider.gameObject.transform.position.x){
			PlayerStateManager.PlayerState = PlayerStateManager.PlayerStates.Collecting; //PLAYERSTATE
			if(PlayerAnimationController.PlayerAnimIsFinished){
				PlayerAnimationController.PlayerAnimIsFinished = false;
				PlayerStateManager.PlayerState = PlayerStateManager.PlayerStates.Idle; //PLAYERSTATE
				var key = collider.gameObject.GetComponent<InteractableController>().Key;
				Destroy(collider.gameObject);
				var inWagonObject = Instantiate(Dictionaries.ColKeyToWagonPrefabDict[key], WagonRef.transform.position, Quaternion.identity);
				inWagonObject.transform.parent = WagonRef.transform;
				InteractableType = InteractableTypes.None;
				MouseInputManager.InputLocked = false;
			}
		}
	}
}