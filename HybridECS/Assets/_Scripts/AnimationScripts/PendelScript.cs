﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PendelScript : MonoBehaviour {

    public GameObject slow;
    public void SlowAnim()
    {
        anim22.Play("SlowObjectAnim");
    }

    Animator anim;
    Animator anim22; 

    //SUBSCRIPTIONS
    private void OnEnable()
    {
        //InteractableManager.OnAllObjectsPlaced += StartAnim;
        GoToStartController.OnCharacterAtStart += StartAnim;
    }
    private void OnDisable()
    {
        //InteractableManager.OnAllObjectsPlaced -= StartAnim;
        GoToStartController.OnCharacterAtStart -= StartAnim;
    }

	// Use this for initialization
	void Start () {

        anim = GetComponent<Animator>();
        anim22 = slow.GetComponent<Animator>(); 
		
	}
	
	// Update is called once per frame
	// void Update () {

    //     if (GameManager.GameState == GameManager.GameStates.ChainReaction)
    //         anim.Play("PendelReactionAnim");
		
	// }

    private void StartAnim(){
        anim.Play("PendelReactionAnim");
    }
}
