﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowObjAudioController : MonoBehaviour {
	AudioSource Audio;
	public float MaxVolume = 1;
	public float MinVolume = 0;
	public float FadeOutSpeed = 2f;
	public float FadeInSpeed = 1f;
	bool FadeIn;
	bool FadeOut;
	// Use this for initialization
	void Start () {
		Audio = GetComponent<AudioSource>();
	}
	
	private void Update()
	{
		if (FadeIn){
			Audio.volume = Mathf.Lerp(Audio.volume, MaxVolume, FadeInSpeed * Time.deltaTime);
		}
		if (FadeOut){
			Audio.volume = Mathf.Lerp(Audio.volume, MinVolume, FadeOutSpeed * Time.deltaTime);
		}
	}
	void WindchimesPlay(){
		Audio.Play();
	}

	void WindchimesFadeIn(){
		Audio.volume = MinVolume;
		FadeIn = true;
	}

	void WindchimesFadeOut(){
		FadeIn = false;
		FadeOut = true;
	}
}
