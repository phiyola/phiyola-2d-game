﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastObjAudioController : MonoBehaviour {
	public AudioClip[] HandPanNotes;
	AudioSource Audio;
	// Use this for initialization
	void Start () {
		Audio = GetComponent<AudioSource>();
	}
	
	void HandPan1(){
		Audio.clip = HandPanNotes[0];
		Audio.Play();
	}
	void HandPan2(){
		Audio.clip = HandPanNotes[1];
		Audio.Play();
	}
	void HandPan3(){
		Audio.clip = HandPanNotes[2];
		Audio.Play();
	}
	void HandPan4(){
		Audio.clip = HandPanNotes[3];
		Audio.Play();
	}
	void HandPan5(){
		Audio.clip = HandPanNotes[4];
		Audio.Play();
	}
	void HandPan6(){
		Audio.clip = HandPanNotes[5];
		Audio.Play();
	}
}
