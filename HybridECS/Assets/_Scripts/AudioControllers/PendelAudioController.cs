﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PendelAudioController : MonoBehaviour {
	AudioSource[] AudioSources;
	AudioSource VierklangSource;
	AudioSource GlockenGSource;
	AudioSource GlockenASource;
	AudioSource GlockenBSource;
	AudioSource GlockenCSource;

	// Use this for initialization
	void Start () {
		AudioSources = GetComponents<AudioSource>();
		VierklangSource = AudioSources[0];
		GlockenGSource = AudioSources[1];
		GlockenASource = AudioSources[2];
		GlockenBSource = AudioSources[3];
		GlockenCSource = AudioSources[4];
	}
	
	void PlayVierklang(){
		VierklangSource.Play();
	}
	void PlayGlockenG(){
		GlockenGSource.Play();
	}
	void PlayGlockenA(){
		GlockenASource.Play();
	}
	void PlayGlockenB(){
		GlockenBSource.Play();
	}
	void PlayGlockenC(){
		GlockenCSource.Play();
	}
}
