﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour
{
    public static Settings Instance;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        if (Instance != this)
            Destroy(gameObject);
    }

    [Header("Player Settings")]
    public float Speed = 2f;

    [Header("Wagon Settings")]
    public float DragDistance = 1.5f;

    [Header("TouchInput Settings")]
    public float MaxTapDuration = 0.2f;
    public float HoldDuration = 0.8f;
    public float OverlapCircleRadius = 0.3f; //The radius of the circle that checks if the touched position overlaps with an interactable object
    public int ObjectsMaskedLayer = 9; //see GroundMaskedLayer

    [Header("GlueToGround Settings")]
    public int GroundMaskedLayer = 8; //TODO: to adjust which layer is going to be masked. This will be removed and hardcoded to the layer the ground objects are on.
	public float GroundBufferDistance; //Gives room for the raycast to hit something underneath the player sprite's anchor.

    [Header("Camera Settings")]
    public float FollowSpeed = 3f;
    public float VerticalOffset = 8f; //Offsets to place the character according to the rule of thirds
    public float HorizontalOffset = 5f;
    public float RegularFOV = 90f;
    public float ReactionCamZoomSpeed = 2f;
    public float ReactionCamMaxFOV = 145f;
    public float ReactionCamPanSpeed = 1.5f;
    public float ReactVerticalOffset = 16f;
}
