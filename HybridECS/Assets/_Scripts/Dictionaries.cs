﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Dictionaries : MonoBehaviour 
{
    //FIELDS
    public static Dictionary<string, GameObject> ColKeyToWagonPrefabDict = new Dictionary<string, GameObject>();
    public static Dictionary<string, GameObject> GapKeyToColDict = new Dictionary<string, GameObject>();

    //The following Arrays are public for testing purposes only.
    public Object[] CollectiblePrefabArray;
    public Object[] CollectibleInWagonPrefabArray;
    public Object[] CollectibleGapArray;
    public Object[] AnimationPrefabArray;

    void Start()
    {
        PopulateArrays();
        PopulateDicts();
    }

    void PopulateArrays()
    {
        CollectiblePrefabArray = Resources.LoadAll("CollectiblePrefabs");
        CollectibleInWagonPrefabArray = Resources.LoadAll("CollectibleInWagonPrefabs");
        CollectibleGapArray = Resources.LoadAll("CollectibleGaps");
        AnimationPrefabArray = Resources.LoadAll("CollectibleAnimations");
    }

    void PopulateDicts()
    {
        int collectibleCount = CollectiblePrefabArray.Length;

        //Collectible Key to Wagon Prefab GameObject
        for (int i = 0; i < collectibleCount; i++)
        {
            GameObject gO = (GameObject) CollectiblePrefabArray[i];
            string gOKey = gO.GetComponent<InteractableController>().Key;
            GameObject wagonGORef = (GameObject) CollectibleInWagonPrefabArray[i];
            ColKeyToWagonPrefabDict.Add(gOKey, wagonGORef);
        }

        //Gap Key to Collectible Prefab GameObject
        for (int i = 0; i < collectibleCount; i++)
        {
            GameObject gO = (GameObject) CollectiblePrefabArray[i];
            GameObject gapGO = (GameObject) CollectibleGapArray[i];
            string gapGOKey = gapGO.GetComponent<InteractableController>().Key;
            GapKeyToColDict.Add(gapGOKey,gO);
        }
    }
}