﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayTest : MonoBehaviour {
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Alpha1)){
			GameManager.GameState = GameManager.GameStates.Playing; //GAMESTATE
			Debug.Log("GameState = Playing");
		}
		if (Input.GetKeyDown(KeyCode.Alpha2)){
			GameManager.GameState = GameManager.GameStates.ChainReaction; //GAMESTATE
			Debug.Log("GameState = ChainReaction");
		}
		if (Input.GetKeyDown(KeyCode.Alpha3)){
			GoToStartController.FirstWaypointReached = false;
			GoToStartController.LastWaypointReached = false;
			GameManager.GameState = GameManager.GameStates.GoToStart; //GAMESTATE
			Debug.Log("GameState = GoToStart");
		}
	}
}
