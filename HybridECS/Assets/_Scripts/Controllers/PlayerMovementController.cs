﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour 
{
	//FIELDS
	public static int IsFacingRightMultiplier = 1;
	private SpriteRenderer PlayerSpriteRef;
	private Vector3 MoveIndicatorPosRef;

	//METHODS
	private void Start()
	{
		//PlayerSpriteRef = GetComponent<SpriteRenderer>();
	}
	private void Update()
	{
		MoveTowardsIndicator();
		RotateTowardsWalkDirection();
	}

	//Moves the Player towards the Indicator with slight smoothing towards the end.
	//Movetowards is a specialized mathf.lerp.
	void MoveTowardsIndicator()
	{
	    if (GameObject.FindGameObjectWithTag("MoveIndicator") != null)
		{
	        Transform moveIndicatorTransRef = GameObject.FindGameObjectWithTag("MoveIndicator").transform;
	        if (transform.position.x != moveIndicatorTransRef.position.x)
			{
                transform.position = Vector3.MoveTowards(transform.position, moveIndicatorTransRef.position, Settings.Instance.Speed * Time.deltaTime);
				PlayerStateManager.PlayerState = PlayerStateManager.PlayerStates.Walking; //PLAYERSTATE
			}
			else
			{
				Destroy(GameObject.FindGameObjectWithTag("MoveIndicator"));
				PlayerStateManager.PlayerState = PlayerStateManager.PlayerStates.Idle; //PLAYERSTATE
			}
        }
    }
	void RotateTowardsWalkDirection() //Rotates the player sprite towards the direction it's moving.
	{
		if(GameObject.FindGameObjectWithTag("MoveIndicator") != null){
			MoveIndicatorPosRef = GameObject.FindGameObjectWithTag("MoveIndicator").transform.position;
			if (MoveIndicatorPosRef.x > GameObject.FindGameObjectWithTag("Player").transform.position.x){
				transform.rotation = Quaternion.Euler(0,0,0);
				//PlayerSpriteRef.flipX = false; //FIXME: Adjust for animations: animator.Play(WalkAnimRight);
				IsFacingRightMultiplier = 1;
			}
			if (MoveIndicatorPosRef.x < GameObject.FindGameObjectWithTag("Player").transform.position.x){
				transform.rotation = Quaternion.Euler(0,180,0);
				//PlayerSpriteRef.flipX = true; //FIXME: Adjust for animations: animator.Play(WalkAnimLeft);
				IsFacingRightMultiplier = -1;
			}
		}
	}
}
