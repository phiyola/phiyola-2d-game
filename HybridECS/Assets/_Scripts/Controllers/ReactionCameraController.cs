﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionCameraController : MonoBehaviour 
{
	Camera CamRef;
	private void Start()
	{
		CamRef = GetComponent<Camera>();
	}
	private void Update()
	{
		IncreaseFOV();
		CenterCamera();
	}
	private void IncreaseFOV(){
		CamRef.fieldOfView = Mathf.Lerp(CamRef.fieldOfView, Settings.Instance.ReactionCamMaxFOV, Settings.Instance.ReactionCamZoomSpeed * Time.deltaTime);
	}
	private void CenterCamera(){
		transform.position = Vector3.Lerp(transform.position, new Vector3(0,Settings.Instance.ReactVerticalOffset,-10), Settings.Instance.ReactionCamPanSpeed * Time.deltaTime);
	}
}
