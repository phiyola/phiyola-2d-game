﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationController : MonoBehaviour 
{
	//FIELDS
	private Animator AnimatorRef;
	public static bool PlayerAnimIsFinished;

	//METHODS
	void Start () {
		AnimatorRef = GetComponent<Animator>();
	}

	void Update () {
		int PlayerState = (int) PlayerStateManager.PlayerState;
		AnimatorRef.SetInteger("PlayerState", PlayerState);
	}

	void SetFinishedBool(){
		PlayerAnimIsFinished = true; //TODO: Set the bool to false wherever this bool is referenced but at least one tick later.
	}
}
