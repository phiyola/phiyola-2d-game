﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToStartController : MonoBehaviour 
{
	//FIELDS
	public static bool FirstWaypointReached;
	public static bool LastWaypointReached;

	//EVENTS
	public delegate void CharacterAtStartAction();
	public static event CharacterAtStartAction OnCharacterAtStart;

	//METHODS
	private void Update()
	{
		GoToStart();
	}
	private void GoToStart(){
		if (GameManager.GameState == GameManager.GameStates.GoToStart){
			if (!FirstWaypointReached){
				if(transform.position.x != GameObject.FindGameObjectWithTag("FirstWaypoint").transform.position.x){
					transform.position += new Vector3((GameObject.FindGameObjectWithTag("FirstWaypoint").transform.position - transform.position).normalized.x, 0, 0) * Settings.Instance.Speed * Time.deltaTime;
				}
				else{
					FirstWaypointReached = true;
				}
			}
			else if (FirstWaypointReached && !LastWaypointReached){
				if (transform.position.x != GameObject.FindGameObjectWithTag("LastWaypoint").transform.position.x){
					Debug.Log("moving towards lastwaypoint");
					transform.position += new Vector3((GameObject.FindGameObjectWithTag("LastWaypoint").transform.position - transform.position).normalized.x, 0, 0) * Settings.Instance.Speed * Time.deltaTime;
				}
				else{
					GameManager.GameState = GameManager.GameStates.ChainReaction;
					OnCharacterAtStart();
					LastWaypointReached = true;
				}
			}
		}
	}
}
