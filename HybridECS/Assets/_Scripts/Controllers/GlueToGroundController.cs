﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlueToGroundController : MonoBehaviour 
{
	//METHODS
	private void Start() //this is so that the objects already spawn at their intended location.
	{
		GlueToGround();
	}

	void Update () {
		GlueToGround();
	}

	void GlueToGround() //Casts rays downwards and upwards and sets the objects y position to whatever was hit.
	{ 
		if (GetRayHit(Vector2.down))
			transform.position -= new Vector3(0, GetRayHit(Vector2.down).distance - Settings.Instance.GroundBufferDistance);
		if (GetRayHit(Vector2.up))
			transform.position += new Vector3(0, GetRayHit(Vector2.up).distance + Settings.Instance.GroundBufferDistance);
	}

	private RaycastHit2D GetRayHit(Vector2 _direction) //Returns a ray hit with a direction attribute.
	{
		RaycastHit2D rayHit;
		rayHit = Physics2D.Raycast(transform.position, _direction, Mathf.Infinity, 1 << Settings.Instance.GroundMaskedLayer);
		return rayHit;
	}
}
