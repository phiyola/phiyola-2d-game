﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableController : MonoBehaviour
{
    public enum Types{
        Part,
        Gap,
        InWagon
    }
    public Types Type;
    public string Key;
    public bool Placed;
}
