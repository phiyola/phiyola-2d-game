﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WagonController : MonoBehaviour
{
    public List<string> KeyList;

    public List<string> GetKeyList()
    {
        ClearKeyList();
        foreach (var interactableController in GetComponentsInChildren<InteractableController>())
        {
            KeyList.Add(interactableController.Key);
        }
        return KeyList;
    }

    public void ClearKeyList()
    {
        KeyList.Clear();
    }
}
