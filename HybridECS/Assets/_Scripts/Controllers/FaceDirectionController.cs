﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceDirectionController : MonoBehaviour {
	private SpriteRenderer PlayerSpriteRef;
	private Vector3 MoveIndicatorPosRef;
	// Use this for initialization
	void Start () {
		PlayerSpriteRef = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		RotateTowardsWalkDirection();
	}
	void RotateTowardsWalkDirection() //Rotates the player sprite towards the direction it's moving.
	{
		if(GameObject.FindGameObjectWithTag("MoveIndicator") != null){
			MoveIndicatorPosRef = GameObject.FindGameObjectWithTag("MoveIndicator").transform.position;
			if (MoveIndicatorPosRef.x > GameObject.FindGameObjectWithTag("Player").transform.position.x){
				PlayerSpriteRef.flipX = false; //FIXME: Adjust for animations: animator.Play(WalkAnimRight);
			}
			if (MoveIndicatorPosRef.x < GameObject.FindGameObjectWithTag("Player").transform.position.x){
				PlayerSpriteRef.flipX = true; //FIXME: Adjust for animations: animator.Play(WalkAnimLeft);
			}
		}
	}
}
