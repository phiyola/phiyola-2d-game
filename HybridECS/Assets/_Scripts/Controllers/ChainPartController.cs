﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainPartController : MonoBehaviour
{
    //FIELDS
    private ChainReactionManager ChainReactManagerRef;
    private Animator Anim;
    private string AnimIndex;

	//METHODS
	void Start ()
	{
	    ChainReactManagerRef = GameObject.FindGameObjectWithTag("Manager").GetComponent<ChainReactionManager>();
	    Anim = GetComponent<Animator>();
	}

    public bool HasFinished()
    {
        return true;
    }
    public void TriggerAnimation()
    {
        //TODO: play attached animation logic
        Anim.Play(Anim.GetComponent<Animation>().name);
        if (HasFinished())
        {
            ChainReactManagerRef.TriggerNextAnimation();
        }
    }
}
