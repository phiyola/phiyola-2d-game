﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothCameraController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
		SmoothFollow();
		ReadjustFOV();
	}

	//METHODS
	void SmoothFollow() //Interpolates between the camera's current position and the player position. 
						//Has offsets that will always position the character according to the rule of thirds.
						//Also reacts to the direction the player is walking in.
	{
		var playerPos = GameObject.FindGameObjectWithTag("Player").transform.position;
		var target = new Vector3(playerPos.x + Settings.Instance.HorizontalOffset * PlayerMovementController.IsFacingRightMultiplier, playerPos.y + Settings.Instance.VerticalOffset, -10);
		transform.position = Vector3.Lerp(transform.position, target, Settings.Instance.FollowSpeed * Time.deltaTime);
	}
	void ReadjustFOV(){
		Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, Settings.Instance.RegularFOV, Settings.Instance.FollowSpeed * Time.deltaTime);
	}
}
