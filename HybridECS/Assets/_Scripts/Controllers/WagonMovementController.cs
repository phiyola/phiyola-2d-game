﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WagonMovementController : MonoBehaviour {
	GameObject PlayerRef;
	// Use this for initialization
	void Start () {
		PlayerRef = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
		BalloonFollowPlayer();
	}

	private void BalloonFollowPlayer(){
		if (Mathf.Abs(PlayerRef.transform.position.x - transform.position.x) >= Settings.Instance.DragDistance){
			transform.position = Vector3.MoveTowards(transform.position, PlayerRef.transform.position, Settings.Instance.Speed * Time.deltaTime);
		}
	}
}
