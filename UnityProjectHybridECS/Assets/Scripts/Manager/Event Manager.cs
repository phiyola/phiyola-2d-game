﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour 
{
	public delegate void TapAction();
	public static event OnTap;

	public delegate void HoldAction();
	public static event OnHold;

	public delegate void GapAction();
	public static event OnGap;

	public delegate void PartAction();
	public static event OnPart;
}
